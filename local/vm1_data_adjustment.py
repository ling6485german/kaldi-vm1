import operator

# Sort utt2spk
utt2spk_dict = {}
with open('data/test/utt2spk', 'r') as utt2spk:
    for line in utt2spk:
        uid_sid = line.split(' ')
        uid, sid = uid_sid[0], uid_sid[1]
        tmpList = uid.split('_')
        new_uid = '_'.join([tmpList[2], tmpList[0], tmpList[1]])
        utt2spk_dict[new_uid] = uid_sid[1]

sorted_utt2spk = sorted(utt2spk_dict.items(), key=operator.itemgetter(0))

with open('data/test/utt2spk_sorted', 'w') as new_file:
    for t in sorted_utt2spk:
        new_file.write("%s %s" % (t[0], t[1]))

# Sort spk2utt
spk2utt_dict = {}
with open('data/test/spk2utt', 'r') as spk2utt:
    for line in spk2utt:
        spk = line.split(' ')[0]
        spk2utt_dict[spk] = line

sorted_spk2utt = sorted(spk2utt_dict.items(), key=operator.itemgetter(0))

with open('data/test/spk2utt_sorted', 'w') as new_file:
    for t in sorted_spk2utt:
        new_file.write(t[1])
