#!/bin/bash

set -e

# Step 1
# Nothing to do here

# Step 2 
VM=/projects/speech/corpus/VM1/
VM_DOC=./vm1-doc/

# Step 3
# Nothing to do here

# Step 4
# Call the next line with the directory where the RM data is. This should
# contain the various VM1 data directories.
local/vm1_data_prep.py $VM $VM_DOC

# Step 5
utils/prepare_lang.sh data/local/dict '!SIL' data/local/lang data/lang
mkdir -p data/local/tmp/
utils/make_unigram_grammar.pl < data/test/raw_text > data/local/tmp/G.txt

# Step 6 grammar
# Creates data/lang/G.fst
local/vm1_prepare_grammar.sh      # Traditional RM grammar (bigram word-pair)

# Step 7 features
# mfccdir should be some place with a largish disk where you
# want to store MFCC features.   You can make a soft link if you want.
# Use a subdirectory.
if [ ! -d mfcc ]; then
  mkdir mfcc
fi

featdir=mfcc
# Run stuff locally
train_cmd=run.pl

for x in test train; do
  steps/make_mfcc.sh --nj 8 --cmd "$train_cmd" data/$x exp/make_feat/$x $featdir
  steps/compute_cmvn_stats.sh data/$x exp/make_feat/$x $featdir
done

# Step 8
# No work necessary

# Step 9. Start up model with train.1k, which is created in the first step
utils/subset_data_dir.sh data/train 1000 data/train.1k
steps/train_mono.sh --nj 4 --cmd "$train_cmd" data/train.1k data/lang exp/mono

# Step 10. Make HCLG graph for decoding
utils/mkgraph.sh --mono data/lang exp/mono exp/mono/graph

# Step 11.
# Decode the test data. 
decode_cmd=run.pl
steps/decode.sh --config conf/decode.config --nj 8 --cmd "$decode_cmd" \
  exp/mono/graph data/test exp/mono/decode

# Step 12. Align data/train using the 1k model. This creates a model in
# exp/mono_ali.
steps/align_si.sh --nj 8 --cmd "$train_cmd" data/train data/lang exp/mono \
  exp/mon_ali
